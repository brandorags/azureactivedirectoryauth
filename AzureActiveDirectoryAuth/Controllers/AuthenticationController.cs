﻿using System;
using System.Configuration;
using System.Security;
using System.Threading.Tasks;
using System.Web.Http;
using AzureActiveDirectoryAuth.Models;
using Microsoft.Identity.Client;

namespace AzureActiveDirectoryAuth.Controllers
{
    [RoutePrefix("authentication")]
    public class AuthenticationController : ApiController
    {
        [HttpPost]
        [Route("")]
        public async Task<string> Authenticate([FromBody] UserCredential userCredential)
        {
            string accessToken = null;

            var clientId = ConfigurationManager.AppSettings["AzureActiveDirectoryClientId"];
            var authority = ConfigurationManager.AppSettings["AzureActiveDirectoryAuthorityUrl"];
            var scopes = new[] { "user.read" };
            var app = PublicClientApplicationBuilder.Create(clientId).WithAuthority(authority).Build();
            
            try
            {
                var securePassword = new SecureString();
                foreach (var character in userCredential.Password)
                {
                    securePassword.AppendChar(character);
                }

                var result = await app.AcquireTokenByUsernamePassword(scopes, userCredential.Username, securePassword).ExecuteAsync();
                if (result != null)
                {
                    accessToken = result.AccessToken;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return accessToken;
        }
    }
}
