﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AzureActiveDirectoryAuth.Models;
using Newtonsoft.Json;

namespace AzureActiveDirectoryAuth.Controllers
{
    [RoutePrefix("authorization")]
    public class AuthorizationController : ApiController
    {
        [HttpPost]
        [Route("")]
        public async Task<string> Authorize()
        {
            string groups = null;

            var accessToken = GetAccessToken(Request.Headers);
            if (accessToken == null)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            var userGroupIds = (await GetUserGroupIds(accessToken)).ToList();
            if (userGroupIds.Any())
            {
                var groupList = await GetUserGroupNames(userGroupIds, accessToken);
                groups = string.Join(",", groupList);
            }

            return groups;
        }

        private string GetAccessToken(HttpHeaders headers)
        {
            string accessToken = null;

            if (headers.Contains("Authorization"))
            {
                accessToken = headers.GetValues("Authorization").First();
            }

            return accessToken;
        }

        private async Task<IEnumerable<string>> GetUserGroupIds(string accessToken)
        { 
            var userGroupIds = new List<string>();

            using (var client = new HttpClient())
            {
                const string graphApiEndpoint = "/me/getMemberGroups";
                var url = ConfigurationManager.AppSettings["MicrosoftGraphApiBaseUrl"] + graphApiEndpoint;

                client.DefaultRequestHeaders.Add("Authorization", accessToken);
                var json = JsonConvert.SerializeObject(new GroupIdRequest { SecurityEnabledOnly = true });
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await client.PostAsync(url, content);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpResponseException(response.StatusCode);
                }

                var responseContent = await response.Content.ReadAsStringAsync();
                var groupIdResponse = JsonConvert.DeserializeObject<GroupIdResponse>(responseContent);

                userGroupIds.AddRange(groupIdResponse.Value);
            }

            return userGroupIds;
        }

        private async Task<IEnumerable<string>> GetUserGroupNames(IEnumerable<string> userGroupIds, string accessToken)
        {
            var userGroupNames = new List<string>();

            using (var client = new HttpClient())
            {
                const string graphApiEndpoint = "/directoryObjects/getByIds";
                var url = ConfigurationManager.AppSettings["MicrosoftGraphApiBaseUrl"] + graphApiEndpoint;

                client.DefaultRequestHeaders.Add("Authorization", accessToken);
                var json = JsonConvert.SerializeObject(new GroupNameRequest { Ids = userGroupIds, Types = new[] { "group" } });
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await client.PostAsync(url, content);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpResponseException(response.StatusCode);
                }

                var responseContent = await response.Content.ReadAsStringAsync();
                var groupNameResponse = JsonConvert.DeserializeObject<GroupNameResponse>(responseContent);

                var groupNames = groupNameResponse.Value.Select(group => group.DisplayName);

                userGroupNames.AddRange(groupNames);
            }

            return userGroupNames;
        }
    }
}
