﻿using System.Collections.Generic;

namespace AzureActiveDirectoryAuth.Models
{
    public class GroupIdResponse
    {
        public IEnumerable<string> Value { get; set; }
    }
}
