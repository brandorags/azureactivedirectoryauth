﻿namespace AzureActiveDirectoryAuth.Models
{
    public class GroupIdRequest
    {
        public bool SecurityEnabledOnly { get; set; }
    }
}
