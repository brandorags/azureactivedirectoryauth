﻿using System.Collections.Generic;

namespace AzureActiveDirectoryAuth.Models
{
    public class GroupNameResponse
    {
        public IEnumerable<ActiveDirectoryGroup> Value { get; set; }
    }
}
