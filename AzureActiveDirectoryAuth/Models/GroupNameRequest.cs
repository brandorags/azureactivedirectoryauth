﻿using System.Collections.Generic;

namespace AzureActiveDirectoryAuth.Models
{
    public class GroupNameRequest
    {
        public IEnumerable<string> Ids { get; set; }
        public IEnumerable<string> Types { get; set; }
    }
}
