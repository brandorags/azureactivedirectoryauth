﻿namespace AzureActiveDirectoryAuth.Models
{
    public class ActiveDirectoryGroup
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
    }
}
